<?php
/**
 * PmaTask.php
 * @author Revin Roman http://phptime.ru
 */

namespace rmrevin\cookyii\build\tasks;

/**
 * Class PmaTask
 * @package rmrevin\cookyii\build\tasks
 */
class PmaTask extends \cookyii\build\tasks\AbstractTask
{

    /** @var string|null */
    public $basePath;

    /** @var string */
    public $pmaConfigFilename = 'pma.config.inc.php';

    /** @var string|null */
    public $vendorPath;

    /** @var boolean */
    public $pruneCreatedLink = true;

    /** @var string */
    public $linkNameSchema = 'pma-%s';

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (empty($this->vendorPath)) {
            $this->vendorPath = realpath(__DIR__ . '/../..');
        }

        if (empty($this->basePath)) {
            $this->basePath = realpath($this->vendorPath . '/..');
        }

        if (!$this->getFileSystemHelper()->isAbsolutePath($this->pmaConfigFilename)) {
            $this->pmaConfigFilename = $this->basePath . '/' . $this->pmaConfigFilename;
        }

        if (empty($this->pmaConfigFilename)) {
            throw new \InvalidArgumentException('Empty pma config filaname.');
        } elseif (!is_readable($this->pmaConfigFilename)) {
            throw new \RuntimeException('Pma config filaname is not readable.');
        }

        $vendorPath = $this->vendorPath;
        $basePath = $this->basePath;
        $phpmyadminPath = $vendorPath . '/phpmyadmin/phpmyadmin';

        if ($this->pruneCreatedLink) {
            /** @var \Symfony\Component\Finder\SplFileInfo[] $Finder */
            $Finder = (new \Symfony\Component\Finder\Finder())
                ->in($basePath)
                ->filter(function (\SplFileInfo $File) {
                    return $this->isPmaLinkFile($File->getBasename());
                });

            foreach ($Finder as $File) {
                $this->getFileSystemHelper()
                    ->remove($File->getPathname());
            }
        }


        symlink($phpmyadminPath, $basePath . '/' . sprintf($this->linkNameSchema, uniqid()));

        $replacement = [
            '###BLOWFISHSECRET###' => sprintf('%s.%s', uniqid(), rand(1000000, 9999999)),
        ];

        $config = file_get_contents($this->pmaConfigFilename);
        file_put_contents($this->pmaConfigFilename, str_replace(array_keys($replacement), array_values($replacement), $config));

        if (!$this->getFileSystemHelper()->exists($phpmyadminPath . '/config.inc.php')) {
            symlink($this->pmaConfigFilename, $phpmyadminPath . '/config.inc.php');
        }

        return true;
    }

    /**
     * @param string $filename
     * @return boolean
     */
    protected function isPmaLinkFile($filename)
    {
        $pattern = '#^' . sprintf($this->linkNameSchema, '.+') . '$#is';

        return preg_match($pattern, $filename) === 1;
    }
}