<?php
/**
 * build.php
 * @author Revin Roman http://phptime.ru
 */

return [
    'pma' => [
        '.task' => [
            'class' => 'rmrevin\cookyii\build\tasks\PmaTask',
            'basePath' => __DIR__,
            'vendorPath' => realpath(__DIR__ . '/../vendor'),
            'pruneCreatedLink' => true,
        ],
    ],
];